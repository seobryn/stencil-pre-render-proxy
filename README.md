## Stencil JS Prerender Proxy

- The idea of this project is to provide a service that acts as proxy to specific application that use stenciljs components and allow this project to be SEO Friendly for Search Engines.

- This Project is already cutomized for @seobryn Design System, if you want to customize for your own design System, You have to install the Stenciljs Design System to this repo and change the dependency into the `index.js` file.

### Setup

1. ensure that you are using `node v12.22.x` and `yarn v1.22.15` or above.
2. run `yarn` command to install all deps.
3. run `yarn start [TARGET_URL]` to run the server.

- For Example, if i'm trying to provide Pre-render for http://my.application.com, so the start command should be `yarn start http://my.application.com` to setup the proxy target URL

[![paypal](https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=KHZLD2QCSJAXN)
