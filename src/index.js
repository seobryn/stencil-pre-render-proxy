const express = require('express')
const morgan = require('morgan')
const {
  createProxyMiddleware,
  responseInterceptor,
} = require('http-proxy-middleware')
const stencil = require('@seobryn/design-system/hydrate')

const app = express()

const PORT = process.env.PORT || 8081
const TARGET_URL = process.argv[2] || 'http://localhost:3000'

app.use(morgan('dev'))

app.use(
  '/_next/webpack-hmr',
  createProxyMiddleware({
    target: TARGET_URL,
    ws: true,
  })
)

app.get(
  '/_next/*',
  createProxyMiddleware({
    target: TARGET_URL,
    changeOrigin: true,
  })
)

app.get(
  '/*',
  createProxyMiddleware({
    target: TARGET_URL,
    changeOrigin: true,
    selfHandleResponse: true,
    onProxyRes: responseInterceptor(
      async (proxyResBuff, proxyRes, req, res) => {
        const strHtml = proxyResBuff.toString('utf8')
        const parsedResponse = await (
          await stencil.renderToString(strHtml)
        ).html
        return parsedResponse
      }
    ),
  })
)

app.all(
  '/*',
  createProxyMiddleware({
    target: TARGET_URL,
    changeOrigin: true,
  })
)

app.listen(PORT, () => {
  console.log(`Server listen on port ${PORT} `)
})
